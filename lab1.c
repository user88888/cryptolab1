#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define ISFILE "-f\0"
#define CIPHER "-c\0"
#define DECIPHER "-dc\0"
#define PER1 "-p1\0"
#define PER2 "-p2\0"
#define PER3 "-p3\0"
#define KEYWORD "-kword\0"
#define CANS "-cans\0"
#define NOFF "-noff\0"
#define KEY "-k\0"
#define CIPHERFILE "cipher\0"
#define KEYFILE "key\0"
#define DECIPHERFILE "decipher\0"
#define POSITION "-p\0"
#define MROW "-m\0"
#define NCOL "-n\0"
#define SPOFF "-spoff\0"
#define CANSFILE "crypto-analysis\0"

static char *fdata;
static int fsize = 0;
static int exec = 0;
static char noff = 0;
static char spoff = 0;
static char *buffer;
static int bufferSize = 0;
static char *timeBuffer;
static char timeBufferSize = 0;

char readFile(char *filename);
void writeFile();
void cryptoAnalysis();
char onePermut();
char twoPermut(char *word);
char threePermut();
char keyWord(char *word, char pos);
void printFile();
int filelength(FILE *fp);
char isPrime(int n);
char findSmallDiv();
char **newMatrix(int m, int n);
void freeMatrix(char **matrix, int m);
char cmpArgs(char * a0, char * a1);
void printMatrix(char **matrix, char m, char n);
void fillMatrixByTheData(char **matrix, char m, char n, char *data, int size);
void fillDataByTheMatrix(char *data, int size, char **matrix, char m, char n);
void fillArrayByTheRandom(char *nums, char n, char srandoff);
void fillBuffer(char *buffer, int size);
void segmentBuffer(char *buffer, int size, int *segment);
void writeFileFromBuffer(char *buffer, int size, char *filename);
void timeFromBuffer();
void printBuffer(char *buffer, int size, char type);
char **genAlphImage(char *word, char pos);
char decipherTwoPermut(char *word);
char decipherThreePermut(char *m, char *n);
char decipherKeyWord(char *word, char pos);

int main(int argc, char **argv) {

  timeFromBuffer();

  for(int i = 0; i < argc; ++i) {
    if(cmpArgs(argv[i], NOFF)) {
      noff = 1; break;
    }
  }

  for(int i = 0; i < argc; ++i) {
    if(cmpArgs(argv[i], SPOFF)) { 
      spoff = 1; break; 
    }
  }

  if(cmpArgs(argv[1], ISFILE) && readFile(argv[2])) {
    if(cmpArgs(argv[3], CIPHER)) {
      if(cmpArgs(argv[4], PER1)) {
        exec = onePermut();
      }
      else
      if(cmpArgs(argv[4], PER2) && cmpArgs(argv[5], KEY)) {
        exec = twoPermut(argv[6]);
      }
      else
      if(cmpArgs(argv[4], PER3)) {
        exec = threePermut();
      }
      else
      if(cmpArgs(argv[4], KEYWORD) && cmpArgs(argv[6], POSITION)) {
        exec = keyWord(argv[5], atoi(argv[7]));
      }

      if(exec) {
        writeFileFromBuffer(buffer, bufferSize, KEYFILE);
        writeFileFromBuffer(fdata, fsize, CIPHERFILE);
      }
    }

    if(cmpArgs(argv[3], DECIPHER)) {
      if(cmpArgs(argv[4], PER1)) {
        onePermut();
        exec = 1;
      }
      else
      if(cmpArgs(argv[4], PER2) && cmpArgs(argv[5], KEY)) {
        decipherTwoPermut(argv[6]);
        exec = 1;
      }
      else
      if(cmpArgs(argv[4], PER3) && cmpArgs(argv[5], MROW) && cmpArgs(argv[7], NCOL)) {
        decipherThreePermut(argv[6], argv[8]);
        exec = 1;
      }
      else
      if(cmpArgs(argv[4], KEYWORD) && cmpArgs(argv[6], POSITION)) {
        decipherKeyWord(argv[5], atoi(argv[7]));
        exec = 1;
      }

      if(exec) {
        writeFileFromBuffer(fdata, fsize, DECIPHERFILE);
      }
    }

    if(cmpArgs(argv[3], CANS)) {
      cryptoAnalysis();
      writeFileFromBuffer(buffer, bufferSize, CANSFILE);
      exec = 1;
    }

    if(!exec) {
      printf("Bad execution.\n");
    }
  }
  
  if(fsize) free(fdata);
  if(bufferSize) free(buffer);
  if(timeBufferSize) free(timeBuffer);
}

void fillBuffer(char *buffer, int size) {
  while(size) { buffer[--size] = '\0'; }
}

void printBuffer(char *buf, int s, char type) {
  char format[] = {'%', 'c', '\0'};
  format[1] = type;
  printf("Buffer contains:\n");
  printf("---------------\n");
  do { printf(format, *buf++); } while(--s);
  printf("\n---------------\n");
}

void segmentBuffer(char *buffer, int size, int *segment) {
  *segment = 0;
  while(buffer[*segment] != '\0' && *segment < size)
   *segment = (*segment) + 1;
}

int filelength(FILE *fp) {
  int length;
  fseek(fp, 0, SEEK_END);
  length = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  return length;
}

char readFile(char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "r")) == NULL) {
    printf("Can't open file.\n");
    return 0;
  }

  fsize = filelength(fp);
  fdata = malloc(fsize);
  
  if(fdata == NULL) {
    printf("Bad alloc\n");
    return 0;
  }

  char ch;
  for(int i = 0; i < fsize; ++i) {
    if((((ch = fgetc(fp)) == '\n') && noff) || (ch == ' ' && spoff)) {
      --i;
      --fsize;
      continue;
    }
    fdata[i] = ch;
  }

  fclose(fp);

  return 1;
}

void timeFromBuffer() {

  time_t t;
  struct tm *timeinfo;

  time(&t);
  timeinfo = localtime(&t);
  timeBufferSize = asprintf(&timeBuffer,
     ".d.%d.%d.%d.%d.%d.%d",
     timeinfo->tm_year,
     timeinfo->tm_mon,
     timeinfo->tm_mday,
     timeinfo->tm_hour,
     timeinfo->tm_min,
     timeinfo->tm_sec);
}

void writeFileFromBuffer(char *buffer, int size, char *filename) {
  FILE *fp;
  char *fbuffer;
  int bufSize = 0;
  int i, j;
  bufSize = strlen(filename) + timeBufferSize;
  fbuffer = (char *)malloc(bufSize * sizeof(char));

  fillBuffer(fbuffer, bufSize);

  for(i = 0; filename[i] != '\0'; ++i) {
    fbuffer[i] = filename[i];
  }

  for(j = 0; j < timeBufferSize; ++i, ++j) {
    fbuffer[i] = timeBuffer[j];
  }

  if((fp = fopen((char *)fbuffer, "w")) == NULL) {
    printf("Can't create file.\n");
    fclose(fp);
  }

  do {
    fprintf(fp, "%c", *buffer++);
  } while(--size);

  fclose(fp);
  free(fbuffer);
}

void writeFile() {

  FILE *fp;
  char *name = "cipher\0";
  time_t t;
  struct tm *timeinfo;
  char *nbuffer;
  char size;
  int i;

  if(fsize <= 0) {
    printf("Bad buffer.\n"); return;
  }

  time(&t);
  timeinfo = localtime(&t);
  size = asprintf(&nbuffer,
     ".k.%d.d.%d.%d.%d.%d.%d.%d",
     findSmallDiv(),
     timeinfo->tm_year,
     timeinfo->tm_mon,
     timeinfo->tm_mday,
     timeinfo->tm_hour,
     timeinfo->tm_min,
     timeinfo->tm_sec);

  
  for(i = 0; name[i] != '\0'; ++i);
  size += i;

  char *filename = (char*)malloc(size * sizeof(char) + 1);
  
  for(i = 0; name[i] != '\0'; ++i) {
    filename[i] = name[i];
  }

  for(;i < size; ++i) {
    filename[i] = *nbuffer++;
  }

  filename[i] = '\0';

  if((fp = fopen((char *) filename, "w")) == NULL) {
    printf("Can't create file.\n");
    free(filename);
    return;
  }    

  do {
    fprintf(fp,"%c", *fdata++);
  } while(--fsize);

  fclose(fp);
  free(filename);
}

void printFile() {
  if(!fsize) {printf("filesize is zero\n"); return;}
  printf("file contains:\n");
  printf("-------------\n");
  for(int i = 0; i < fsize; ++i) {
    printf("%c", fdata[i]);
  }
  if(noff) printf("\n");
  printf("-------------\n");
}

char isPrime(int n) {
  char primeRes;
  for(int i = 2; i < n/2 + 1; ++i) {
    if(n % i != 0) {
      primeRes = 1; 
    } else {
      primeRes = 0;
      break;
    }
  }
  return primeRes;
}

char findSmallDiv() {
  if(fsize % 2 == 0) return 2;
  
  int n;
  for(int i = 3; i < fsize / 2; ++i) {
    if((n = fsize % i) == 0) return i;
  }  

  return 0;
}

char **newMatrix(int m, int n) {
  char **matrix = (char **)malloc(m * sizeof(char *));
  for(int i = 0; i < m; ++i) {
    matrix[i] = (char *)malloc(n * sizeof(char));
  }
  return matrix;
}

void freeMatrix(char **matrix, int m) {
  do { free(matrix[m-1]); } while(--m);
}

void fillMatrixByTheData(char **matrix, char m, char n, char *data, int size) {
  
  if(m*n != size) {
    printf("Bad m,n of matrix or size of data.\n");
    return;
  }

  if(matrix == NULL) {
    printf("Matrix ptr is null.\n");
    return;
  }

  if(data == NULL) {
    printf("Data ptr is null.\n");
    return;
  }

  for(int i = 0; i < m; ++i) {
    for(int j = 0; j < n; ++j) {
      matrix[i][j] = *data++;
    }
  }

}

void movMatrixColToMatrix(char **matrix, char **matrixR, char m, char n, char col, char ncol) {
  
  if(!(col <= n && ncol <= n)) {
    printf("Move column bad args.\n");
    return;
  }

  for(int i = 0; i < m; ++i) {
    matrixR[i][ncol] = matrix[i][col];
  }
}

void movMatrixRowToMatrix(char **matrix, char **matrixR, char m, char n, char row, char nrow) {
  if(!(row <= m && nrow <= m)) {
    printf("Move row bad args.\n");
    return;
  }
  for(int i = 0; i < n; ++i) {
    matrixR[nrow][i] = matrix[row][i];
  } 
}

void fillDataByTheMatrix(char *data, int size, char **matrix, char m, char n) {
  if(m*n != size) {
    printf("Bad m*n of matrix or size of data.\n");
    return;
  }

  if(matrix == NULL) {
    printf("Matrix ptr is null.\n");
    return;
  }

  if(data == NULL) {
    printf("Data ptr is null.\n");
    return;
  }

  for(int i = 0; i < m; ++i) {
    for(int j = 0; j < n; ++j) {
      *data++ = matrix[i][j];
    }
  }
}

char onePermut() {
  char m, n;
  char elem;
  char **matrix;
  int i, j, k;
  
  if(!isPrime(fsize)) {
    if((n = findSmallDiv()) == 0) {
      printf("Bad small div.\n");
      return 0;
    }
    m = fsize / n;
    matrix = newMatrix(m, n);

    for(i = 0, k = 0; i < m; ++i) {
      for(j = 0; j < n; ++j) {
        matrix[i][j] = *(fdata + k);
        ++k;
      }
    }

    for(i = 0; i < (m / 2); ++i) {
      for(j = 0; j < n; ++j) {
        elem = matrix[i][j];
        matrix[i][j] = matrix[m-1-i][j];
        matrix[m-1-i][j] = elem;
      }
    }

    for(i = 0, k = 0; i < m; ++i) {
      for(j = 0; j < n; ++j) {
        *(fdata + k) = matrix[i][j];
        ++k;
      }
    }
    
    freeMatrix(matrix, m);
  }
  else
  {
    printf("Disaster. Text has prime length.\n");
  }

  bufferSize = asprintf(&buffer, "p1.k.p1^-1");

  return 1;
}

char cmpArgs(char *a0, char *a1) {
  
  while(*a0 != '\0' || *a1 != '\0') {
    if(*a0++ != *a1++) {
      return 0;
    }
  }

  return 1;
}

void printMatrix(char **matrix, char m, char n) {
  printf("Print matrix:\n");
  printf("---------------\n");
  for(int i = 0; i < m; ++i) {
    for(int j = 0; j < n; ++j) {
      printf(" %c ", matrix[i][j] == '\0' ? '_' : matrix[i][j]);
    }
    printf("\n");
  }
  printf("---------------\n");
}

char **genWordMatrix(char *word) {
  char i, j;
  char elem;
  char elem2;
  char n = strlen(word);
  char **wordMatrix = newMatrix(2, n);
  
  for(i = 0; i < n; ++i) {
    wordMatrix[0][i] = word[i];
    wordMatrix[1][i] = i;
  }

  for(i = 0; i < n; ++i) {
    for(j = 0; j < n; ++j) {
      if((i != j) && (wordMatrix[0][i] < wordMatrix[0][j])) {
        elem = wordMatrix[0][j];
        elem2 = wordMatrix[1][j];
        wordMatrix[0][j] = wordMatrix[0][i];
        wordMatrix[1][j] = wordMatrix[1][i];
        wordMatrix[0][i] = elem;
        wordMatrix[1][i] = elem2;
      }
    }
  }

  return wordMatrix;
}

char twoPermut(char *word) {
  char elem;
  char i, j, m, n;
  char **matrix;
  char **matrix1;
  char **wordMatrix;

  if(!isPrime(fsize)) {
    if((n = findSmallDiv()) == 0) {
      printf("Bad small div.\n");
      return 0;
    }
    m = fsize / n;

    for(i = 0; i < n && word[i] != '\0'; ++i); 

    if(n != i) {
      printf("Bad key word.\n");
      return 0;
    }

    wordMatrix = genWordMatrix(word);
    matrix = newMatrix(m, n);
    matrix1 = newMatrix(m, n);
    fillMatrixByTheData(matrix, m, n, fdata, fsize);

    for(i = 0; i < n; ++i) {
      movMatrixColToMatrix(matrix, matrix1, m, n, i, wordMatrix[1][i]);
    }
    
    fillDataByTheMatrix(fdata, fsize, matrix1, m, n);  
    freeMatrix(matrix, m);
    freeMatrix(matrix1, m);
    freeMatrix(wordMatrix, 2);
  }
  else
  {
    printf("Disaster. Text has prime length.\n");
  }

  bufferSize = asprintf(&buffer, "p2.k.%s", word);

  return 1;
}

void fillArrayByTheRandom(char *nums, char n, char srandoff) {

  int i, j;

  if(nums == NULL) {
    printf("Array has null ptr.\n");
    return;
  }

  if(!srandoff) {
    srand(time(NULL));
  } 

  for(i = 0; i < n; ) {
    nums[i] = rand() % n;
    for(j = 0; j < n; ++j) {
      if(i == j) continue;
      if(nums[i] == nums[j]) break;
    }
    if(j == n) ++i;
  }

}

char threePermut() {
  
  char *numsM;
  char *numsN;
  char **matrix;
  char **matrix1;
  char m, n;
  char i, j, r;
  char *p3 = "p3.";
  int seg;
  
  srand(time(NULL));

  if(!isPrime(fsize)) {
    if((n = findSmallDiv()) == 0) {
      printf("Bad small div.\n");
      return 0;
    }
    m = fsize / n;
    matrix = newMatrix(m, n);
    matrix1 = newMatrix(m, n);
    numsM = (char *)malloc(m * sizeof(char));
    numsN = (char *)malloc(n * sizeof(char));
    // do it parallel
    fillMatrixByTheData(matrix, m, n, fdata, fsize);
    fillArrayByTheRandom(numsM, m, 1);
    fillArrayByTheRandom(numsN, n, 1);

    bufferSize = fsize * 3 + strlen(p3) * 3;
    buffer = (char *)malloc(bufferSize * sizeof(char));
    fillBuffer(buffer, bufferSize);
    sprintf(buffer, "%s", p3);
    segmentBuffer(buffer, bufferSize, &seg);
    sprintf(buffer + seg, "%s", "m.");
    segmentBuffer(buffer, bufferSize, &seg);
    // do it parallel
    for(i = 0; i < m; ++i) {
      sprintf(buffer + seg + i, "%d%c", numsM[i], '.');
      //segmentBuffer(buffer, bufferSize, &seg);
    }
    seg += i;
    sprintf(buffer + seg, "%s", ".n.");
    segmentBuffer(buffer, bufferSize, &seg);
    for(i = 0; i < n; ++i) {
      sprintf(buffer + seg + i, "%d%c", numsN[i], '.');
      //segmentBuffer(buffer, bufferSize, &seg);
    }

    for(i = 0; i < n; ++i) {
      movMatrixColToMatrix(matrix, matrix1, m, n, i, numsN[i]);
    }
    printMatrix(matrix1, m , n);
    for(i = 0; i < m; ++i) {
      movMatrixRowToMatrix(matrix1, matrix, m, n, i, numsM[i]);
    }

    fillDataByTheMatrix(fdata, fsize, matrix, m, n);

    freeMatrix(matrix, m);
    freeMatrix(matrix1, m);
    free(numsM);
    free(numsN);
  }
  else 
  {
    printf("Disaster. Text has prime length.\n");
  }

  return 1;
}

char **genAlphImage(char *word, char pos) {
  int i, j, k, t;
  char m = 2;
  char n = 26;
  char **alphMatrix = newMatrix(m, n);
  char bufferSize = strlen(word) + 1;
  char *wordBuffer = (char *)malloc(bufferSize * sizeof(char));
  char *abcBuffer = (char *)malloc((n + 1) * sizeof(char));
  fillBuffer(wordBuffer, bufferSize);
  abcBuffer[n] = '\0';

  for(i = 0; i < n; ++i) {
    alphMatrix[0][i] = i + 97;
    abcBuffer[i] = alphMatrix[0][i];
    alphMatrix[1][i] = 0;
  }
  //alphMatrix[0][i] = ' ';
  //alphMatrix[1][i] = 0;

  for(i = 0; word[i] != '\0'; ++i) {
    wordBuffer[i] = word[i];
  }
  wordBuffer[i] = '\0';

  for(i = 0; wordBuffer[i] != '\0'; ++i) {
    for(j = 0; wordBuffer[j] != '\0'; ++j) {
      if(i == j) continue;
      if(wordBuffer[i] == wordBuffer[j]) wordBuffer[j] = '~';
    }
  }

  for(i = pos, j = 0; wordBuffer[j] != '\0'; ++j) {
    if(i == n) { i ^= i; }
    if(wordBuffer[j] != '~') {
      alphMatrix[1][i] = wordBuffer[j];
      ++i;
    }
  }

  for(k = 0; abcBuffer[k] != '\0'; ++k) {
    for(t = 0; wordBuffer[t] != '\0'; ++t) {
      if(wordBuffer[t] == '~') continue;
      if(wordBuffer[t] == abcBuffer[k]) {
        abcBuffer[k] = '~';
        break;
      }
    }
  }

  for(k = 0; k < n; ++k, ++i) {
    if(i == n) { i ^= i; }
    while(abcBuffer[k] == '~') ++k;
    alphMatrix[1][i] = abcBuffer[k];
  }

  return alphMatrix;
}

char keyWord(char *word, char pos) {
  if(pos > 26) {
    printf("Bad position value.\n");
    return 0;
  }
  int i, j;
  char **matrixIm = genAlphImage(word, pos);
  for(i = 0; i < fsize; ++i) {
    for(j = 0; j < 26; ++j) {
      if(fdata[i] == matrixIm[0][j]) {
        fdata[i] = matrixIm[1][j];
        break;
      }
    }
  }

  bufferSize = asprintf(&buffer, "keyword.k.%s.p.%d", word, pos);
  freeMatrix(matrixIm, 2);
  return 1;
}

char decipherTwoPermut(char *word) {
  char elem;
  char i, j, m, n;
  char **matrix;
  char **matrix1;
  char **wordMatrix;

  if(!isPrime(fsize)) {
    if((n = findSmallDiv()) == 0) {
      printf("Bad small div.\n");
      return 0;
    }
    m = fsize / n;

    for(i = 0; i < n && word[i] != '\0'; ++i); 

    if(n != i) {
      printf("Bad key word.\n");
      return 0;
    }

    wordMatrix = genWordMatrix(word);
    matrix = newMatrix(m, n);
    matrix1 = newMatrix(m, n);
    fillMatrixByTheData(matrix, m, n, fdata, fsize);

    for(i = 0; i < n; ++i) {
      movMatrixColToMatrix(matrix, matrix1, m, n, wordMatrix[1][i], i);
    }
    
    fillDataByTheMatrix(fdata, fsize, matrix1, m, n);  
    freeMatrix(matrix, m);
    freeMatrix(matrix1, m);
    freeMatrix(wordMatrix, 2);
  }
  else
  {
    printf("Disaster. Text has prime length.\n");
  }

  return 1;
}

char decipherThreePermut(char m[], char n[]) {
  char sizeM;
  char sizeN;
  char i, j, k, r, f;
  char *numsM;
  char *numsN;
  char acc = 4;
  char reduce[acc];
  char **matrix;
  char **matrix1;

  fillBuffer(reduce, 4);

  sizeM ^= sizeM;
  for(i = 0; m[i] != '\0'; ++i) {
    if(m[i] == '.') ++sizeM;
  }

  printf("sizeM: %d\n", sizeM);

  sizeN ^= sizeN;
  for(j = 0; n[j] != '\0'; ++j) {
    if(n[j] == '.') ++sizeN;
  }

  printf("sizeN: %d\n", sizeN);

  numsM = (char *)malloc(sizeM * sizeof(char));
  numsN = (char *)malloc(sizeN * sizeof(char));
  fillBuffer(numsM, sizeM);
  fillBuffer(numsN, sizeN);

  for(k = 0, r = 0, f = 0; f < sizeM; ++k) {
    if(m[k] != '.') {
      r = k;
      while((m[k] != '.') && ((k - r) < acc)) {
        reduce[k - r] = m[k];
        ++k;
      }
      if(m[k] == '.') {
        numsM[f] = atoi(reduce);
        ++f;
      }
      else {
        printf("Bad args sequence of permutation.\n");
        return 0;
      }
      fillBuffer(reduce, acc);
    }
  }

  for(k = 0, r = 0, f = 0; f < sizeN; ++k) {
    if(n[k] != '.') {
      r = k;
      while((n[k] != '.') && ((k - r) < acc)) {
        reduce[k - r] = n[k];
        ++k;
      }
      if(n[k] == '.') {
        numsN[f] = atoi(reduce);
        ++f;
      }
      else {
        printf("Bad args sequence of permutation.\n");
        return 0;
      }
      fillBuffer(reduce, acc);
    }
  }

  matrix = newMatrix(sizeM, sizeN);
  matrix1 = newMatrix(sizeM, sizeN);
  fillMatrixByTheData(matrix, sizeM, sizeN, fdata, fsize);
  for(i = 0; i < sizeM; ++i) {
    movMatrixRowToMatrix(matrix, matrix1, sizeM, sizeN, numsM[i], i);
  }
  for(i = 0; i < sizeN; ++i) {
    movMatrixColToMatrix(matrix1, matrix, sizeM, sizeN, numsN[i], i);
  }

  fillDataByTheMatrix(fdata, fsize, matrix, sizeM, sizeN);
  freeMatrix(matrix, sizeM);
  freeMatrix(matrix1, sizeM);
  free(numsM);
  free(numsN);

  return 1;
}

char decipherKeyWord(char *word, char pos) {
  if(pos > 26) {
    printf("Bad position value.\n");
    return 0;
  }
  int i, j;
  char **matrixIm = genAlphImage(word, pos);
  for(i = 0; i < fsize; ++i) {
    for(j = 0; j < 26; ++j) {
      if(fdata[i] == matrixIm[1][j]) {
        fdata[i] = matrixIm[0][j];
        break;
      }
    }
  }
  printMatrix(matrixIm, 2, 26);
  freeMatrix(matrixIm, 2);
  return 1;
}

void cryptoAnalysis() {
  int i, j, k, r;
  char **matrixSymQuant;
  char *unicsym = (char *)malloc(fsize * sizeof(char));

  for(i = 0; i < fsize; ++i) {
    unicsym[i] = fdata[i];
  }

  k = 0;
  for(i = 0; i < fsize; ++i) {
    for(j = 0; j < fsize; ++j) {
      if((i == j) || (unicsym[i] == '~')) {
        continue;
      }
      if(unicsym[i] == unicsym[j]) {
        unicsym[j] = '~';
        ++k;
      }
    }
  }

  k = fsize - k;
  matrixSymQuant = newMatrix(k, 2);

  for(i = 0, j = 0; i < fsize; ++i) {
    if(unicsym[i] != '~') {
      matrixSymQuant[j][0] = unicsym[i];
      matrixSymQuant[j][1] = 0;
      ++j;
    }
  }

  for(i = 0; i < k; ++i) {
    for(j = 0; j < fsize; ++j) {
      if(matrixSymQuant[i][0] == fdata[j]) {
        ++matrixSymQuant[i][1];
      }
    }
  }

  bufferSize = k * 15;
  buffer = (char *)malloc(bufferSize * sizeof(char));
  fillBuffer(buffer, bufferSize);
  sprintf(buffer, "%s", "sym-quant\n");
  segmentBuffer(buffer, bufferSize, &r);
  for(i = 0; i < k; ++i) {
    sprintf(buffer + r, "%d%s%d%s", 
        matrixSymQuant[i][0],
        " - ",
        matrixSymQuant[i][1],
        "\n");
    segmentBuffer(buffer, bufferSize, &r);
  }
  bufferSize = r;
  freeMatrix(matrixSymQuant, k);
  free(unicsym);
}
